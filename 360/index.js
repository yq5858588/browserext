
var myNotifications = window.webkitNotifications;    
var notification = myNotifications.createNotification(
    '48.png', // icon url - can be relative
    'Hello!', // notification title
    'Lorem ipsum...' // notification body text
);

// 或者创建一个 HTML 通知：
var notification = myNotifications.createHTMLNotification(
    'notification.html' // html url - can be relative
);

// 显示通知
notification.show();